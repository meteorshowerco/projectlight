//
//  ViewController.swift
//  EVCharge
//
//  Created by Jeffery Kuo on 9/4/17.
//  Copyright © 2017 jk. All rights reserved.
//

import UIKit
import MapKit
import Font_Awesome_Swift
import CircleProgressBar
import HMKit
import AutoAPI
import Alamofire
import SwiftyJSON

class ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate, LocalDeviceDelegate, LinkDelegate {

    // info
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var charge: CircleProgressBar!
    @IBOutlet weak var chargePercentageLabel: UILabel!
    @IBOutlet weak var chargeRangeLabel: UILabel!
    @IBOutlet weak var chargeTimeLabel: UILabel!
    @IBOutlet weak var totalMilesLabel: UILabel!
    @IBOutlet weak var tripALabel: UILabel!
    @IBOutlet weak var findChargeButton: UIButton!
    @IBOutlet weak var houseTabLabel: UILabel!
    @IBOutlet weak var mapTabLabel: UILabel!
    @IBOutlet weak var callTabLabel: UILabel!
    @IBOutlet weak var chargeTabLabel: UILabel!
    @IBOutlet weak var mileageTabLabel: UILabel!
    
    // music
    @IBOutlet weak var musicView: UIView!
    @IBOutlet weak var rewindButton: UILabel!
    @IBOutlet weak var pauseButton: UILabel!
    @IBOutlet weak var forwardButton: UILabel!
    
    // map
    @IBOutlet weak var mapView: MKMapView!

    // directions
    @IBOutlet weak var directionsView: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var minutesLabel: UILabel!
    @IBOutlet weak var directionIcon: UILabel!
    @IBOutlet weak var streetLabel: UILabel!
    @IBOutlet weak var streetDistanceLabel: UILabel!
    
    // warning
    @IBOutlet weak var warningView: UIView!
    @IBOutlet weak var circularView: UIView!
    @IBOutlet weak var backLabel: UILabel!
    @IBOutlet weak var warningLabel: UILabel!
    @IBOutlet weak var stationTitleLabel: UILabel!
    @IBOutlet weak var stationTypeLabel: UILabel!
    
    var timer: Timer?
    var locationManager: CLLocationManager?
    var serial: (Data)!
    var myLocation: CLLocationCoordinate2D!
    var navigating: Bool = false
    var chargeLowerHappened: Bool = false
    var stationInfo: JSON!
    
    // constants
    let iconSize: CGFloat = 24.0
    
    let timeFormatter: DateFormatter = {
        let tmpFormatter = DateFormatter()
        tmpFormatter.dateFormat = "hh:mm a"
        return tmpFormatter
    }()
    
    let dateFormatter: DateFormatter = {
        let tmpFormatter = DateFormatter()
        tmpFormatter.dateFormat = "MMM d, yyyy"
        return tmpFormatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // info
        setGradientBackground(view: infoView)
        weatherLabel.setFAText(prefixText: "", icon: .FACloud, postfixText: " 65°", size: iconSize)
        timeLabel.text = timeFormatter.string(from: Date())
        timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(self.setTimeOfDate), userInfo: nil, repeats: true)
        dateLabel.text = dateFormatter.string(from: Date())
        
        charge.setProgress(0.3, animated: false)
        charge.progressBarWidth = 10.0
        charge.progressBarProgressColor = UIColor(red: 251.0/255.0, green: 208.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        charge.progressBarTrackColor = UIColor(red: 136.0/255.0, green: 136.0/255.0, blue: 136.0/255.0, alpha: 1.0)
        charge.startAngle = 270.0
        charge.hintViewBackgroundColor = .clear
        charge.setHintTextGenerationBlock { (progress) -> String? in
            return String.init(format: "%.0f%%", arguments: [30.0])
        }
        houseTabLabel.setFAIcon(icon: .FAHome, iconSize: iconSize)
        mapTabLabel.setFAIcon(icon: .FAMapO, iconSize: iconSize)
        callTabLabel.setFAIcon(icon: .FAPhone, iconSize: iconSize)
        chargeTabLabel.setFAIcon(icon: .FABatteryThreeQuarters, iconSize: iconSize)
        mileageTabLabel.setFAIcon(icon: .FAGear, iconSize: iconSize)
        
        // music
        rewindButton.setFAIcon(icon: .FABackward, iconSize: iconSize)
        pauseButton.setFAIcon(icon: .FAPause, iconSize: iconSize)
        forwardButton.setFAIcon(icon: .FAForward, iconSize: iconSize)
        
        // map
        mapView.userTrackingMode = .follow
        mapView.delegate = self
        
        locationManager = CLLocationManager()
        locationManager!.delegate = self
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager!.startUpdatingLocation()
        } else {
            locationManager!.requestWhenInUseAuthorization()
        }
        
        // directions
        directionIcon.setFAIcon(icon: .FAMailReply, iconSize: iconSize)
        distanceLabel.text = "20 miles"
        streetLabel.text = "Decatur St NE"
        minutesLabel.text = "20"
        streetDistanceLabel.text = "0.4 miles"
        
        // warning
        setGradientBackground(view: warningView)
        warningView.isHidden = true
        backLabel.setFAText(prefixText: "", icon: .FAChevronLeft, postfixText: " Back", size: iconSize)
        warningLabel.setFAIcon(icon: .FAExclamationTriangle, iconSize: 72.0)
        circularView.layer.borderWidth = 1
        circularView.layer.borderColor = UIColor.white.cgColor
        circularView.layer.cornerRadius = circularView.frame.height / 2.0
        
        pauseButton.isUserInteractionEnabled = true
        let gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(startBroadcast))
        pauseButton.addGestureRecognizer(gesture)
        
        initialiseMercedesSDK()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
    }
    
    func startDrive() {
        navigating = true
        
        let destination = CLLocationCoordinate2D(latitude: 33.783981, longitude: -84.390376)
        let request = MKDirectionsRequest()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: myLocation, addressDictionary: nil))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: destination, addressDictionary: nil))
        request.requestsAlternateRoutes = false
        request.transportType = .automobile
        
        let directions = MKDirections(request: request)
        
        directions.calculate { [unowned self] response, error in
            guard let unwrappedResponse = response else { return }
            
            for route in unwrappedResponse.routes {
                self.mapView.add(route.polyline)
                self.mapView.setVisibleMapRect(route.polyline.boundingMapRect, animated: false)
                self.mapView.setZoomByDelta(delta: 2, animated: false)
            }
            
            let newPin = MKPointAnnotation()
            newPin.coordinate = destination
            self.mapView.addAnnotation(newPin)
        }
    }
    
    @IBAction func searchForStation(_ sender: Any) {
        backLabel.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(backPressed))
        backLabel.addGestureRecognizer(tapGesture)
        
        Alamofire.request("https://developer.nrel.gov/api/alt-fuel-stations/v1.json?api_key=90L2UFMCCG4RI03vQb8AQBnVO2h28C3VHx0aRElc&fuel_type=E85,ELEC&zip=30363&limit=1").responseJSON { response in
            
            if let json = response.result.value {
                print("***")
                print("\(json)")
                
                self.stationInfo = JSON(json)
                let station = self.stationInfo["fuel_stations"].arrayValue[0]
                self.stationTitleLabel.text = station["station_name"].stringValue
                self.stationTypeLabel.text = station["ev_network"].stringValue
                self.warningView.isHidden = false
            }
        }
    }
    
    func backPressed() {
        warningView.isHidden = true
    }
    
    @IBAction func addStop(_ sender: Any) {
        backPressed()
        // reroute
        distanceLabel.text = ".75 miles"
        streetLabel.text = "M.L.K. Jr. Dr SW"
        minutesLabel.text = "5"
        streetDistanceLabel.text = "0.4 miles"
        
        driveToStation()
    }
    
    func driveToStation() {
//        let destination = CLLocationCoordinate2D(latitude: 33.7865497, longitude: -84.3949197)
        let station = self.stationInfo["fuel_stations"].arrayValue[0]
        guard let latitude = NumberFormatter().number(from: station["latitude"].stringValue) else { return }
        guard let longitude = NumberFormatter().number(from: station["longitude"].stringValue) else { return }
        let destination = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
        
        let request = MKDirectionsRequest()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: myLocation, addressDictionary: nil))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: destination, addressDictionary: nil))
        request.requestsAlternateRoutes = false
        request.transportType = .automobile
        
        let directions = MKDirections(request: request)
        
        directions.calculate { [unowned self] response, error in
            guard let unwrappedResponse = response else { return }
            
            let overlays = self.mapView.overlays
            self.mapView.removeOverlays(overlays)
            
            for route in unwrappedResponse.routes {
                self.mapView.add(route.polyline)
            }
            
            let newPin = MKPointAnnotation()
            newPin.coordinate = destination
            self.mapView.addAnnotation(newPin)
            
            self.appendSecondLeg()
        }
    }
    
    func appendSecondLeg() {
        let destination = CLLocationCoordinate2D(latitude: 33.783981, longitude: -84.390376)
//        let stationOrigin = CLLocationCoordinate2D(latitude: 33.7865497, longitude: -84.3949197)
        let station = self.stationInfo["fuel_stations"].arrayValue[0]
        guard let latitude = NumberFormatter().number(from: station["latitude"].stringValue) else { return }
        guard let longitude = NumberFormatter().number(from: station["longitude"].stringValue) else { return }
        let stationOrigin = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
        
        let request = MKDirectionsRequest()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: stationOrigin, addressDictionary: nil))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: destination, addressDictionary: nil))
        request.requestsAlternateRoutes = false
        request.transportType = .automobile
        
        let directions = MKDirections(request: request)
        
        directions.calculate { [unowned self] response, error in
            guard let unwrappedResponse = response else { return }
            
            for route in unwrappedResponse.routes {
                self.mapView.add(route.polyline)
            }
        }
    }
    
    private func initialiseMercedesSDK() {
        // initialise certificate
        do {
            try LocalDevice.shared.initialise(
                deviceCertificate: "dGVzdLFJth6aopoPU2+TCl/Xw0Ib7gF/OBp7cs6Xtjm9y4i5jni53JfcHfiCGKPNm3X1SEjXjhLsSLRh+jH5cKtrgOXt267N7YDOsK8ejA4X/zDqectcUuhZJ22R830fd6vvlzImesVKZinKrjmkNYGsqtnCcmI2VyEiIp6yjA+HPSZeCwlVzQvurSs5It30PREugXIm/JTA",
                devicePrivateKey: "xUbtd4+o1J5fKz5L/j25D6T0ITd08si8dE57YMzrLr0=",
                issuerPublicKey: "ba7+0hwIN9hOWSEbGdvqTN8yfSI7i0Tf40CA0TaSGCoWGlGBZ7rjpF1Z7fHEGkHKBqiWQYZ5fKHH0uXJrtucyw=="
            )
            
            LocalDevice.shared.delegate = self
            try LocalDevice.shared.startBroadcasting()
        }
        catch {
            // Handle the error
            print("Invalid initialisation parameters, please double check the snippet.")
        }
        
        do {
            try Telematics.downloadAccessCertificate(accessToken: "GFOvdA1_FuO3UkID4RzXSixmzD6fI-6WUznuA3NTm5QPj6HEsevt6fTo_WnNRCZWVFEASOCsG6UR7ZWqwgbgEJEHQaPa1MYFTzF-NhiO_QgvSE1k5A9nn6oiuvXtgnK9YQ") {
                switch $0 {
                case .failure(_):
                    // Handle the failure
                    break
                case .success(let vehicleSerialData):
                    // Handle the success
                    self.serial = vehicleSerialData
                    self.scanBatteryCharge()
                    break
                }
            }
        }
        catch {
            // Handle the error
        }
    }
    
    func startBroadcast() {
        do {
            print("Attempting to start broadcast.")
            LocalDevice.shared.delegate = self
            try LocalDevice.shared.startBroadcasting()
        }
        catch {
            // Handle the error
            print("Unable to start the bluetooth broadcast.")
        }
    }
    
    func scanBatteryCharge() {
        do {
            try Telematics.sendCommand(AutoAPI.ChargingCommand.getStateBytes, vehicleSerial: serial) { response in
                
                if case Telematics.TelematicsRequestResult.success(let data) = response {
                    guard let data = data else {
                        return // fail
                    }
                    
                    guard let chargeResponse = AutoAPI.parseIncomingCommand(data)?.value as? AutoAPI.ChargingCommand.Response else {
                        print("Failed to cast the response appropriately.")
                        return
                    }
                    
                    print("Got data back \(chargeResponse)")
                    
                    DispatchQueue.main.async {
                        let newChargeVal = CGFloat(Double(chargeResponse.batteryLevel)/100.0)
                        self.charge.setProgress(newChargeVal, animated: true)
                        
                        self.charge.setHintTextGenerationBlock { (progress) -> String? in
                            return String.init(format: "%.0f%%", arguments: [newChargeVal*100.0])
                        }
                        
                        self.chargeRangeLabel.text = "\(chargeResponse.estimatedRange)km range"
                    }
                }
                else {
                    print("Failed to lock the doors \(response).")
                }
            }
        }
        catch {
            print("Failed to send command")
        }
    }
    
    func setTimeOfDate() {
        timeLabel.text = timeFormatter.string(from: Date())
    }

    // MARK: CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            print("NotDetermined")
        case .restricted:
            print("Restricted")
        case .denied:
            print("Denied")
        case .authorizedAlways:
            print("AuthorizedAlways")
        case .authorizedWhenInUse:
            print("AuthorizedWhenInUse")
            locationManager!.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last!
        
        myLocation = location.coordinate
        if !navigating {
            startDrive()
        }

        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        if !navigating {
                self.mapView.setRegion(region, animated: true)
        }

    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to initialize GPS: ", error.localizedDescription)
    }
    
    // MARK : MKMapViewDelegate
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
        renderer.strokeColor = UIColor(red: 0.0/255.0, green: 122.0/255.0, blue: 250.0/255.0, alpha: 1.0)
        return renderer
    }
    
    // MARK : Utility functions
    func setGradientBackground(view: UIView) {
        let colorTop =  UIColor(red: 72.0/255.0, green: 72.0/255.0, blue: 72.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 46.0/255.0, green: 46.0/255.0, blue: 46.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = view.bounds
        
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    // MARK : Bluetooth link
    func localDevice(didReceiveLink link: Link) {
        print("didReceiveLink")
        link.delegate = self
    }
    
    func localDevice(didLoseLink link: Link) {
        print("lost link")
        link.delegate = nil
    }
    
    func localDevice(stateChanged state: LocalDeviceState, oldState: LocalDeviceState) {
        print("stateChanged")
    }
    
    func link(_ link: Link, didReceiveAuthorisationRequest serialNumber: [UInt8], approve: @escaping LinkDelegate.Approve, timeout: TimeInterval) {
        do {
            // Approving without user input
            try approve()
        } catch {
            print("Authorisation request timed out")
        }
    }
    
    func link(_ link: Link, didReceiveCommand bytes: [UInt8]) {
        if let chargeResponse = AutoAPI.parseIncomingCommand(bytes)?.value as? AutoAPI.ChargingCommand.Response {
            
            print("Got the charge command \(chargeResponse).")

            if chargeResponse.batteryLevel <= 20 && !chargeLowerHappened {
                chargeLowerHappened = true
                searchForStation(0)
            }
            
            DispatchQueue.main.async {
                let newChargeVal = CGFloat(Double(chargeResponse.batteryLevel)/100.0)
                self.charge.setProgress(newChargeVal, animated: true)
                
                self.charge.setHintTextGenerationBlock { (progress) -> String? in
                    return String.init(format: "%.0f%%", arguments: [newChargeVal*100.0])
                }
                
                self.chargeRangeLabel.text = "\(chargeResponse.estimatedRange)km range"
            }
        }
    }
    
    func link(_ link: Link, stateDidChange oldState: LinkState) {
    }
    
}

extension MKMapView {
    
    // delta is the zoom factor
    // 2 will zoom out x2
    // .5 will zoom in by x2
    
    func setZoomByDelta(delta: Double, animated: Bool) {
        var _region = region;
        var _span = region.span;
        _span.latitudeDelta *= delta;
        _span.longitudeDelta *= delta;
        _region.span = _span;
        
        setRegion(_region, animated: animated)
    }
}
