# README #

### What is this repository for? ###

Project Light is the project that team Meteor Shower came up with for the Mercedes-Benz hackathon challenge.  It is aimed at easing the pain point of navigation for electric vehicle owners by reactively providing guidance and wayfinding when an EV needs a charge in order to reach its final destination.

### How do I get set up? ###

* iOS app for iPad.
* Open the xcworkspace file located in the project as it uses CocoaPods.
* Best viewed on a 10.5" iPad in Landscape orientation.
* Best used as demonstration if device GPS is located in USA, preferably Atlanta, GA.
* To simulate the real time demonstration of the reactive app, the app/iPad must be linked to the car emulator through Bluetooth in Chrome. Set the charge level of the vehicle to a value of 30%.
* Once the app is launched on the iPad, connect through Chrome to the iPad. Once the emulator and the iPad are paired, change the charge level of the emulator vehicle to 20%, and the app will react to the change. This triggers our low battery level alert, which will then add in a real charging station (via API) to your journey, along with your destination.

### Feedback ###

Questions or comments? email us at contact@meteorshower.co
